package models

import (
	commonmodels "go-todo/common/models"
	"go-todo/utils"

	"gorm.io/gorm"
)

type User struct {
	commonmodels.Base
	Email    string `gorm:"type:varchar(100);unique_index"`
	Password string
	Name     string
	Todos    []Todo `gorm:"foreignKey:UserID;references:ID"`
}

func (user *User) BeforeSave(tx *gorm.DB) (err error) {
	hashed, err := utils.GetPasswordUtil().HashPassword(user.Password)
	user.Password = hashed
	return
}
