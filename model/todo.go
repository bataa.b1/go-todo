package models

import (
	commonmodels "go-todo/common/models"

	"github.com/google/uuid"
)

type Todo struct {
	// gorm.Model
	commonmodels.Base
	Task   string
	Status bool
	UserID uuid.UUID `gorm:"type:uuid;"`
}
