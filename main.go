package main

import (
	"go-todo/common"
	"go-todo/database"
	models "go-todo/model"
	routes "go-todo/routes"
	"log"

	"github.com/go-playground/validator"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal("Error loading .env file")
	}

	api := echo.New()
	api.Validator = &common.CustomValidator{Validator: validator.New()}

	api.Use(middleware.Logger())
	api.Use(middleware.Recover())

	api.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	db := database.GetInstance()
	db.AutoMigrate(&models.User{}, &models.Todo{})

	routes.DefineApiRoute(api)

	server := echo.New()

	server.Any("/*", func(c echo.Context) (err error) {
		req := c.Request()
		res := c.Response()
		if req.URL.Path == "/todo" {
			api.ServeHTTP(res, req)
		}

		return
	})
	server.Logger.Fatal(server.Start(":1323"))

}
