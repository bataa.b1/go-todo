package services

import (
	"go-todo/database"
	models "go-todo/model"

	"github.com/google/uuid"
)

func GetTodo(userID uuid.UUID) *[]models.Todo {
	db := database.GetInstance()

	var todo []models.Todo
	err := db.Find(&todo, "user_id = ?", userID).Error

	if err == nil {
		return &todo
	}

	return nil
}

func FindTodoByID(Id uuid.UUID, userID uuid.UUID) *models.Todo {
	db := database.GetInstance()

	var todo models.Todo

	err := db.First(&todo, "ID=? AND user_id = ?", Id, userID).Error

	if err == nil {
		return &todo
	}
	return nil
}

func AddTodo(task string, userID uuid.UUID) *models.Todo {
	db := database.GetInstance()

	todo := models.Todo{
		Task:   task,
		Status: false,
		UserID: userID,
	}

	db.Create(&todo)

	return &todo
}

func UpdateTodo(Id uuid.UUID, task string, status bool, userID uuid.UUID) *models.Todo {
	var todo models.Todo

	db := database.GetInstance()
	err := db.First(&todo, "ID = ? AND user_id = ?", Id, userID).Error

	if err == nil {
		todo.Task = task
		todo.Status = status

		db.Save(&todo)
		return &todo
	}
	return nil
}

func DeleteTodoByID(Id uuid.UUID, userID uuid.UUID) *models.Todo {
	db := database.GetInstance()

	var todo models.Todo

	err := db.First(&todo, "ID = ? AND user_id = ?", Id, userID).Error

	if err == nil {
		db.Delete(&todo)
		return &todo
	}

	return nil
}
