package services

import (
	"go-todo/database"
	models "go-todo/model"
	"sync"
)

type userService struct{}

var singleton UserService
var once sync.Once

func (u *userService) FindUserByEmail(email string) *models.User {
	db := database.GetInstance()
	var user models.User
	err := db.First(&user, "email = ?", email).Error
	if err == nil {
		return &user
	}
	return nil
}

func (u *userService) AddUser(email string, password string) *models.User {
	user := models.User{
		Email:    email,
		Password: password,
	}

	db := database.GetInstance()
	db.Create(&user)

	return &user
}

type UserService interface {
	FindUserByEmail(email string) *models.User
	AddUser(email string, password string) *models.User
}

func GetUserService() UserService {
	if singleton != nil {
		return singleton
	}

	once.Do(func() {
		singleton = &userService{}
	})

	return singleton
}

func SetUsersService(service UserService) UserService {
	original := singleton
	singleton = service
	return original
}
