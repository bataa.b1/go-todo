package routes

import (
	"fmt"
	"go-todo/common"

	apitodo "go-todo/controller/todo"
	apiuser "go-todo/controller/user"

	"github.com/labstack/echo/v4"
)

func DefineApiRoute(e *echo.Echo) {
	controllers := []common.Controller{
		apitodo.TodoController{},
		apiuser.AuthController{},
	}

	var routes []common.Route

	for _, controller := range controllers {
		routes = append(routes, controller.Routes()...)
	}

	api := e.Group("/todo")

	for _, route := range routes {
		switch route.Method {
		case echo.POST:
			{
				api.POST(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.GET:
			{
				api.GET(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.DELETE:
			{
				api.DELETE(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.PUT:
			{
				api.PUT(route.Path, route.Handler, route.Middleware...)
				break
			}
		case echo.PATCH:
			{
				api.PATCH(route.Path, route.Handler, route.Middleware...)
				break
			}
		}
	}
	fmt.Println(routes)
}
