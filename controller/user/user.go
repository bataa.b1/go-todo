package apiuser

import (
	"fmt"
	"go-todo/common"
	"go-todo/services"
	"go-todo/utils"
	"net/http"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

type (
	AuthController      struct{}
	RegisterUserRequest struct {
		Email    string `json:"email" from:"email" query:"email" validate:"email,required"`
		Password string `json:"password" validate:"required"`
	}
	LoginRequest struct {
		Email    string `json:"email" from:"email" query:"email" validate:"email,required"`
		Password string `json:"password" validate:"required"`
	}
)

func (contoller AuthController) Routes() []common.Route {
	return []common.Route{
		{
			Method:  echo.POST,
			Path:    "/auth/login",
			Handler: contoller.Login,
		},
		{
			Method:  echo.POST,
			Path:    "/auth/register",
			Handler: contoller.Register,
		},
		{
			Method:     echo.GET,
			Path:       "/auth/profile",
			Handler:    contoller.Profile,
			Middleware: []echo.MiddlewareFunc{common.JwtMiddleWare()},
		},
	}
}

func (controller AuthController) Profile(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	token := user.Claims.(*common.JwtCustomClaims)
	return c.JSON(http.StatusOK, token)
}

func (controller AuthController) Register(cnt echo.Context) error {
	params := new(RegisterUserRequest)

	if err := cnt.Bind(params); err != nil {
		fmt.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	if err := cnt.Validate(params); err != nil {
		fmt.Print(err)
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	if user := services.GetUserService().FindUserByEmail(params.Email); user != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "email is already used")
	}

	user := services.GetUserService().AddUser(params.Email, params.Password)

	fmt.Println("success")

	return cnt.JSON(http.StatusOK, user)
}

func (controller AuthController) Login(cnt echo.Context) error {
	params := new(LoginRequest)

	if err := cnt.Bind(params); err != nil {
		fmt.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	if err := cnt.Validate(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	user := services.GetUserService().FindUserByEmail(params.Email)

	if user == nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Invalid email or password")
	}

	if matched := utils.GetPasswordUtil().CheckPasswordHash(params.Password, user.Password); !matched {
		return echo.NewHTTPError(http.StatusUnauthorized, "Invalid email or password")
	}

	token, _ := GetAuthService().GetAccessToken(user)

	return cnt.JSON(http.StatusOK, map[string]string{
		"token": token,
	})

}
