package apiuser

import (
	"go-todo/common"
	"go-todo/config"
	models "go-todo/model"

	"os"
	"sync"
	"time"

	"github.com/golang-jwt/jwt"
)

type authService struct{}

var singleton AuthService
var once sync.Once

func GetAuthService() AuthService {
	once.Do(func() {
		singleton = &authService{}
	})
	return singleton
}

type AuthService interface {
	GetAccessToken(user *models.User) (string, error)
}

func (s *authService) GetAccessToken(user *models.User) (string, error) {
	claims := &common.JwtCustomClaims{
		Email: user.Email,
		Id:    user.ID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * config.TokenExpiresIn).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte(os.Getenv("JWT_SECRET_KEY")))
}
