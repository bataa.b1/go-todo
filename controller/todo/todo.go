package apitodo

import (
	"go-todo/common"
	"go-todo/services"
	"net/http"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

type (
	TodoController struct {
	}

	CreateTodoRequest struct {
		Task string `json:"task" form:"task" query:"task" validate:"required"`
	}

	GetTodoRequest struct {
		TodoID uuid.UUID `json:"todoID" form:"todoID" query:"todoID" param:"todoID" validate:"required"`
	}

	UpdateTodoRequest struct {
		TodoID uuid.UUID `json:"todoID" form:"todoID" query:"todoID" param:"todoID" validate:"required"`
		Task   string    `json:"task" form:"task" query:"task" validate:"required"`
		Status *bool     `json:"status" form:"status" query:"status" validate:"required"`
	}
)

func (controller TodoController) Routes() []common.Route {
	return []common.Route{
		{
			Method:     echo.GET,
			Path:       "/todos/:todoID",
			Handler:    controller.getTodoById,
			Middleware: []echo.MiddlewareFunc{common.JwtMiddleWare()},
		},
		{
			Method:     echo.PATCH,
			Path:       "/todos/:todoID",
			Handler:    controller.updateTodo,
			Middleware: []echo.MiddlewareFunc{common.JwtMiddleWare()},
		},
		{
			Method:     echo.DELETE,
			Path:       "/todos/:todoID",
			Handler:    controller.deleteTodo,
			Middleware: []echo.MiddlewareFunc{common.JwtMiddleWare()},
		},
		{
			Method:     echo.GET,
			Path:       "/todos",
			Handler:    controller.getTodo,
			Middleware: []echo.MiddlewareFunc{common.JwtMiddleWare()},
		},
		{
			Method:     echo.POST,
			Path:       "/todos",
			Handler:    controller.addTodo,
			Middleware: []echo.MiddlewareFunc{common.JwtMiddleWare()},
		},
	}
}

func (controller TodoController) getTodo(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	token := user.Claims.(*common.JwtCustomClaims)
	todos := services.GetTodo(token.Id)
	return c.JSON(http.StatusOK, todos)
}

func (controller TodoController) addTodo(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	token := user.Claims.(*common.JwtCustomClaims)
	params := new(CreateTodoRequest)
	if err := c.Bind(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	if err := c.Validate(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	todo := services.AddTodo(params.Task, token.Id)
	return c.JSON(http.StatusOK, todo)
}

func (controller TodoController) getTodoById(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	token := user.Claims.(*common.JwtCustomClaims)
	params := new(GetTodoRequest)
	if err := c.Bind(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	if err := c.Validate(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	todo := services.FindTodoByID(params.TodoID, token.Id)
	if todo == nil {
		return echo.NewHTTPError(http.StatusBadRequest, "todo not found")
	}
	return c.JSON(http.StatusOK, todo)
}

func (controller TodoController) updateTodo(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	token := user.Claims.(*common.JwtCustomClaims)
	params := new(UpdateTodoRequest)
	if err := c.Bind(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	if err := c.Validate(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	todo := services.UpdateTodo(params.TodoID, params.Task, *params.Status, token.Id)
	if todo == nil {
		return echo.NewHTTPError(http.StatusBadRequest, "todo not found")
	}
	return c.JSON(http.StatusOK, todo)
}

func (controller TodoController) deleteTodo(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	token := user.Claims.(*common.JwtCustomClaims)
	params := new(GetTodoRequest)
	if err := c.Bind(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	if err := c.Validate(params); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}
	todo := services.DeleteTodoByID(params.TodoID, token.Id)
	if todo == nil {
		return echo.NewHTTPError(http.StatusBadRequest, "todo not found")
	}
	return c.JSON(http.StatusOK, "success")
}
